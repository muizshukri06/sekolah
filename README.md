<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# LARAVEL 8 : Student System

## Senarai Kandungan  

* [Keperluan](#keperluan)  
* [Pemasangan](#pemasangan)  

## <a name="keperluan"></a>Keperluan  

* Laragon (Full Edition) - https://laragon.org/download/index.html
* Apache
* MySQL 8
* PHP ^7.3.0 - https://windows.php.net/download#php-7.4
* Composer - https://getcomposer.org/
* Git - https://git-scm.com/
* Laravel ^8.0 - https://laravel.com/docs/8.x
* Bootstrap 4 - https://getbootstrap.com/docs/4.0/getting-started/download/
* AdminLTE v3.2.0 (Template) - https://github.com/ColorlibHQ/AdminLTE/releases

## <a name="pemasangan"></a>Pemasangan   

1. Muat turun kod sumber ```git clone https://gitlab.com/muizshukri06/sekolah.git```  
1. Bukak terminal dan pergi ke directory path project > taip ```composer update```
1. Create Database ```sekolah``` pada pangkalan data yang ingin digunakan
1. Sila copy file .env.example dan rename kepada .env ```[folder_project]/.env```  
1. Kemaskini database di fail .env ```[folder_project]/.env```  
        
        # Contoh konfigurasi database
         DB_CONNECTION=mysql
         DB_HOST=[host_db]
         DB_PORT=3306
         DB_DATABASE=[database_name]
         DB_USERNAME=[username]
         DB_PASSWORD=[password]
         
1. Bukak terminal dan pergi ke directory path project > taip ```php artisan key:generate```
1. Bukak terminal dan pergi ke directory path project > taip  ```php artisan migrate```, untuk menghasilkan table yang diperlukan oleh sistem.
1. Bukak terminal dan pergi ke directory path project > taip  ```php artisan serve```, untuk run sistem.

