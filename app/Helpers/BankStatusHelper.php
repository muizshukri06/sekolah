<?php 

namespace App\Helpers;

use App\Models\Assetlookup;

class BankStatusHelper {

    public static function countFrom($data) {
        $currentPage = null;
        $perPage = null;

        $currentPage = $data->currentPage();
        $perPage= $data->perPage();
        if($currentPage !=null) {
            $currentPage=$currentPage-1;
            $startCount=$currentPage*$perPage;
        } else {
            $startCount=0;
        }
        return $startCount;
    }

    public static function getNegeri() {
        $data = Assetlookup::where(['category'=>'negeri'])->get();

        $plucked = $data->pluck('name','id');
        return $plucked;
    }

}