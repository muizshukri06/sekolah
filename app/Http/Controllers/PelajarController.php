<?php

namespace App\Http\Controllers;

use App\Models\Pelajar;
use Illuminate\Http\Request;
use App\Helpers\BankStatusHelper;

class PelajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    //    echo 'ayam';
        $d['title'] = 'Senarai Pelajar';
        $d['model'] = Pelajar::paginate(4);

       return view('pelajar.senarai',$d);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $title = 'Tambah Pelajar';
        $getNegeri = BankStatusHelper::getNegeri();

        return view('pelajar.index',compact('title','getNegeri'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());

        $message = [
            'nama.required'=>'Sila isi nama anda!',
            'umur.required'=>'Sila isi umur anda!',
            'jantina.required'=>'Sila isi jantina anda!',
            'negeri.required'=>'Sila isi negeri anda!'
        ];

        $validate = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'jantina' => 'required',
            'negeri' => 'required',
        ],$message);

        if(!$validate) {
            return redirect()->withInput();
        };
// dd($validate);
        Pelajar::create($validate);

        // $model = new Pelajar;
        // $model->nama = $request->name;
        // $model->umur = $request->umur;
        // $model->jantina = $request->jantina;
        // $model->save();
        return redirect()->route('pelajarindex');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $d['model'] = Pelajar::where(['id' => $id])->first();
        
        return view('pelajar.papar',$d);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $d['model'] = Pelajar::find($id);
        $d['getNegeri'] = BankStatusHelper::getNegeri();

        return view('pelajar.edit',$d);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->all());
        $message = [
            'nama.required'=>'Sila isi nama anda!'
        ];

        $validate = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'jantina' => 'required',
            'negeri' => 'required',
        ],$message);

        // dd($validate);
        if(!$validate) {
            return redirect()->withInput();
        };
// dd($validate);
        $model = Pelajar::find($id);
        $model->update($validate);
        // $model = new Pelajar;
        // $model->nama = $request->name;
        // $model->umur = $request->umur;
        // $model->jantina = $request->jantina;
        // $model->save();

        return redirect()->route('pelajarindex');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Pelajar::destroy($id);

        return redirect()->route('pelajarindex');

    }
}
