<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    //
    public function login(){

        return view('auth.login');
    }

    public function store(Request $request){

        $message = [
            'email.unique'=>'Email telah didaftar',
            'password.min'=>'Password terlalu pendek'
        ];
        $validate = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:5|max:255'
        ],$message);

        if(!$validate) {
            return redirect()->withInput();
        };

        $validate['password'] = Hash::make($validate['password']);

        User::create($validate);

        return redirect('login')->with('success','Telah berjaya didaftar!');
    }

    public function register(){

        return view('auth.register');
    }

    public function authenticate(Request $request){

        $credential = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if(Auth::attempt($credential)){
            $request->session()->regenerate();

            return redirect()->intended('pelajar');
        }

        return back()->with('error','Maklumat tidak tepat!');
    }

    public function logout(Request $request){

        Auth::logout();
 
        $request->session()->invalidate();
     
        $request->session()->regenerateToken();
     
        return redirect('/login');
    }
}
