<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pelajar extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'nama','umur','jantina','negeri'
    ];


    public function negeris(){
        return $this->belongsTo('App\Models\Assetlookup','negeri','id');
    }

    

}
