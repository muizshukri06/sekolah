<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assetlookup extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','code','category'
    ];

    public function negeris(){
        return $this->hasMany('App\Models\Pelajar','negeri');
    }
}
