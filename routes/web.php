<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PelajarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PelajarController::class, 'index'])->name('pelajarindex')->middleware('auth');
Route::get('/pelajar', [PelajarController::class, 'index'])->name('pelajarindex')->middleware('auth');
Route::get('/pelajar-tambah', [PelajarController::class, 'create'])->name('pelajartambah');
Route::get('/pelajar-edit/{id}', [PelajarController::class, 'edit'])->name('pelajaredit');
Route::get('/pelajar-papar/{id}', [PelajarController::class, 'show'])->name('pelajarpapar');
Route::put('/pelajar-update/{id}', [PelajarController::class, 'update'])->name('pelajarupdate');
Route::delete('/pelajar-delete/{id}', [PelajarController::class, 'destroy'])->name('pelajarpadam');
Route::post('/pelajar-store', [PelajarController::class, 'store'])->name('pelajarstore');

// Route::resource('/pelajar',PelajarController::class);


Route::get('/login', [LoginController::class, 'login'])->name('login')->middleware('guest');
Route::get('/register', [LoginController::class, 'register'])->name('register');
Route::post('/register-store', [LoginController::class, 'store'])->name('registerstore');
Route::post('/authenticate', [LoginController::class, 'authenticate'])->name('authenticate');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');