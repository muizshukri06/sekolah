@php
    use App\Helpers\BankStatusHelper;
@endphp

@extends('app.layout')

@section('content')
    @php
        $bil = BankStatusHelper::countFrom($model);
    @endphp

    <div>
        <a href="{{ route('pelajartambah') }}" class="btn btn-success float-right"><i class="fas fa-user-plus"></i> Tambah </a>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Jantina</th>
                <th scope="col">Aktiviti</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($model as $value)
                <tr>
                    <th scope="row" class="col-md-2">{{ ++$bil }}</th>
                    <td class="col-md-3">{{ $value->nama }}</td>
                    <td class="col-md-2">{{ $value->umur }}</td>
                    <td class="col-md-2">{{ $value->jantina }}</td>
                    <td class="d-flex">
                        <a href="{{ route('pelajaredit', $value->id) }}" class="btn btn-warning mr-2"><i class="fas fa-user-edit"></i> Kemaskini</a>
                        <a href="{{ route('pelajarpapar',$value->id) }}" class="btn btn-primary mr-2"><i class="fas fa-info-circle"></i> Papar</a>
                        <form action="{{ route('pelajarpadam', $value->id) }}" method="post">
                            @csrf
                            @method('delete')

                            <button class="btn btn-danger deleteConfirm" type="submit"><i class="fas fa-trash-alt"></i> Padam</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="d-flex justify-content-end mt-2">
        {{ $model->links() }}
    </div>

    {{-- Alert : Delete Confirmation --}}
@section('script')
    <script type="text/javascript">
        $('.deleteConfirm').click(function(event) {
            var form = $(this).parents('form');
            event.preventDefault();
            Swal.fire({
                title: 'Adakah anda pasti?',
                text: 'Data pelajar akan dibuang dari sistem',
                icon: 'warning',
                showDenyButton: true,
                confirmButtonColor: '#3085d6',
                denyButtonColor: '#d33',
                confirmButtonText: 'Ya',
                denylButtonText: 'Tidak',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Berjaya',
                    'Data pelajar telah dibuang dari sistem',
                    'success'
                    )
                    form.submit()
                } else if (result.isDenied) {
                    Swal.fire(
                    'Tindakan Dibatalkan',
                    'Tiada tindakan dilakukan',
                    'error'
                    )
                }
            })
        });
    </script>
@endsection
@endsection
