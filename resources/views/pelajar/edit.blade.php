@extends('app.layout')

@section('content')

<form action="{{ route('pelajarupdate',$model->id) }}" method="post">
    @csrf
    @method('put')

    @include('pelajar.form')


    <button type="submit" class="btn btn-primary float-right">Hantar</button>
  <a href="{{ route('pelajarindex') }}" class="btn btn-secondary float-right mr-2">Kembali</a>
</form>

@endsection